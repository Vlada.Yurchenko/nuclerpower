let socket = new WebSocket('ws://vps.yojji.io', 'vlada_yurchenko');

const requestForCheck = {
  action: 'check', 
  'lever1': 0, 
  'lever2': 1, 
  stateId: 'some-state-id' 
}

const powerOff = {
  action: 'powerOff', 
  stateId: 'some-state-id'
}

let states = {
  0: false,
  1: false,
  2: false,
  3: false
}

let stateToTurnOff = true;

socket.onopen = function() {
  console.log('connection is set');
};

socket.onmessage = function(event) {  
  const message = JSON.parse(event.data);

  if (message.newState === 'poweredOff') {
    console.log('token:', message.token);
    socket.close();

    return;
  }

  if (message.newState === 'poweredOn') {
    stateToTurnOff = false;
  }

  if (message.pulled >= 0) {
    const lever = message.pulled;
    states[lever] = !states[lever];
    requestForCheck.stateId = message.stateId;

    socket.send(JSON.stringify(requestForCheck));
  }

  if (message.same) {
    states[requestForCheck['lever2']] = states[requestForCheck['lever1']];

    if(requestForCheck['lever2'] < 3) {
      requestForCheck['lever2'] += 1;
    }

    const leversSameFlag = Object.values(states).every(el => el === stateToTurnOff);

    if (leversSameFlag) {
      powerOff.stateId = message.stateId;

      socket.send(JSON.stringify(powerOff));
    }
  }
};
